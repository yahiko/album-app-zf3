<?php
declare( strict_types = 1 );

namespace Album\Controller;

use Album\Form\AlbumForm;
use Album\Model\Album;
use Album\Service\AlbumService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class AlbumController extends AbstractActionController
{
	/**
	 * @var AlbumService
	 */
	private $service;

	public function __construct( AlbumService $service )
	{
		$this->service = $service;
	}

	public function indexAction()
	{
		return new ViewModel( [
			'albums' => $this->service->find(),
		] );
	}

	public function addAction()
	{
		$form = new AlbumForm;
		$form->get( 'submit' )->setValue( 'Add' );

		$request = $this->getRequest();

		if( ! $request->isPost() ) {
			return [ 'form' => $form ];
		}

		$form->setData( $request->getPost() );

		if( ! $form->isValid() ) {
			return [ 'form' => $form ];
		}

		$data = $form->getData();

		$album = new Album;
		$album->setId( $data[ 'id' ] )
		      ->setArtist( $data[ 'artist' ] )
		      ->setTitle( $data[ 'title' ] );

		$this->service->save( $album );

		return $this->redirect()->toRoute( 'album' );
	}

	public function editAction()
	{
		$id = (int) $this->params()->fromRoute( 'id', 0 );

		if( 0 === $id ) {
			return $this->redirect()->toRoute( 'album', [ 'action' => 'add' ] );
		}

		$album = $this->service->getAlbumById( $id );
		if( null === $album ) {
			return $this->redirect()->toRoute( 'album', [ 'action' => 'index' ] );
		}

		$form = new AlbumForm();
		$form->bind( $album );
		$form->get( 'submit' )->setAttribute( 'value', 'Edit' );

		$request  = $this->getRequest();
		$viewData = [ 'id' => $id, 'form' => $form ];

		if( ! $request->isPost() ) {
			return $viewData;
		}

		$form->setData( $request->getPost() );

		if( ! $form->isValid() ) {
			return $viewData;
		}

		$this->service->save( $album );

		// Redirect to album list
		return $this->redirect()->toRoute( 'album', [ 'action' => 'index' ] );
	}

	public function deleteAction()
	{
		$request = $this->getRequest();

		$id = (int) $this->params()->fromRoute( 'id', 0 );

		if( $request->isPost() ) {
			$id = (int) $request->getPost( 'id' );
		}

		if( ! $id ) {
			return $this->redirect()->toRoute( 'album' );
		}

		$album = $this->service->getAlbumById( $id );
		if( null === $album ) {
			return $this->redirect()->toRoute( 'album' );
		}

		if( $request->isPost() ) {
			$del = $request->getPost( 'del', 'No' );

			if( $del == 'Yes' ) {
				$this->service->delete( $album );
			}

			// Redirect to list of albums
			return $this->redirect()->toRoute( 'album' );
		}

		return [
			'id'    => $id,
			'album' => $album,
		];
	}
}