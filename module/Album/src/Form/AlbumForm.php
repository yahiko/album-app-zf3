<?php
declare( strict_types = 1 );

namespace Album\Form;

use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use Zend\Filter\ToInt;
use Zend\Form\Form;
use Zend\Hydrator\ClassMethods;
use Zend\InputFilter\InputFilter;
use Zend\Validator\StringLength;

class AlbumForm extends Form
{
	public function __construct( $name = null )
	{
		// We will ignore the name provided to the constructor
		parent::__construct( 'album' );

		$this->setHydrator( new ClassMethods() );

		$this->add( [
			'name' => 'id',
			'type' => 'hidden',
		] );
		$this->add( [
			'name'    => 'title',
			'type'    => 'text',
			'options' => [
				'label' => 'Title',
			],
		] );
		$this->add( [
			'name'    => 'artist',
			'type'    => 'text',
			'options' => [
				'label' => 'Artist',
			],
		] );
		$this->add( [
			'name'       => 'submit',
			'type'       => 'submit',
			'attributes' => [
				'value' => 'Go',
				'id'    => 'submitbutton',
			],
		] );
	}

	public function getInputFilter()
	{
		if( $this->filter ) {
			return $this->filter;
		}

		$inputFilter = new InputFilter();

		$inputFilter->add( [
			'name'     => 'id',
			'required' => true,
			'filters'  => [
				[ 'name' => ToInt::class ],
			],
		] );

		$inputFilter->add( [
			'name'       => 'artist',
			'required'   => true,
			'filters'    => [
				[ 'name' => StripTags::class ],
				[ 'name' => StringTrim::class ],
			],
			'validators' => [
				[
					'name'    => StringLength::class,
					'options' => [
						'encoding' => 'UTF-8',
						'min'      => 1,
						'max'      => 100,
					],
				],
			],
		] );

		$inputFilter->add( [
			'name'       => 'title',
			'required'   => true,
			'filters'    => [
				[ 'name' => StripTags::class ],
				[ 'name' => StringTrim::class ],
			],
			'validators' => [
				[
					'name'    => StringLength::class,
					'options' => [
						'encoding' => 'UTF-8',
						'min'      => 1,
						'max'      => 100,
					],
				],
			],
		] );

		$this->filter = $inputFilter;

		return $this->filter;
	}
}