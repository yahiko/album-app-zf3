<?php
declare( strict_types = 1 );

namespace Album\Service;

use Album\Mapper\AlbumMapper;
use Album\Model\Album;

class AlbumService
{
	/**
	 * @var AlbumMapper
	 */
	private $mapper;

	public function __construct( AlbumMapper $mapper )
	{
		$this->mapper = $mapper;
	}

	/**
	 * @param integer $id
	 *
	 * @return Album
	 */
	public function getAlbumById( int $id ) : ?Album
	{
		$albums = $this->mapper->find( [ 'id' => $id ]);

		if( empty( $albums ) ) {
			return null;
		}

		if( count( $albums ) > 1 ) {
			throw new \DomainException( 'Impossibru! Can only have one album with the same id' ) ;
		}

		return $albums[0];
	}

	/**
	 * @param array $searchParams
	 *
	 * @return Album[]
	 */
	public function find( array $searchParams = array() )
	{
		return $this->mapper->find( $searchParams );
	}

	public function save( Album $album )
	{
		if( empty( $album->getId() ) ) {
			$this->mapper->insert( $album );
		} else {
			$this->mapper->update( $album );
		}
	}

	public function delete( Album $album )
	{
		$this->mapper->delete( (int) $album->getId() );
	}
}