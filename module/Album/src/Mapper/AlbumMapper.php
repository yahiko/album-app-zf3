<?php
declare( strict_types = 1 );

namespace Album\Mapper;

use Album\Model\Album;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\AdapterInterface;

class AlbumMapper
{
	/**
	 * @var AdapterInterface
	 */
	private $db;

	public function __construct( AdapterInterface $db )
	{
		$this->db = $db;
	}

	public function find( array $searchParams )
	{
		$where = [];
		$bind  = [];

		foreach( $searchParams as $name => $value ) {
			switch( $name ) {
				case 'id':
					$where[]      = 'id = :id';
					$bind[ 'id' ] = $value;
					break;
				case 'title':
					$where[]         = 'title = :title';
					$bind[ 'title' ] = $value;
					break;
				case 'artist':
					$where[]          = 'artist = :artist';
					$bind[ 'artist' ] = $value;
					break;
				default:
					throw new \DomainException( sprintf(
						'Invalid search parameter "%s"',
						$name
					) );
			}
		}

		$sql = '
			SELECT
				id,
				artist,
				title
			FROM
				album
		';

		if( ! empty( $where ) ) {
			$sql .= ' where ' . implode( ' AND ', $where );
		}

		$stmt   = $this->db->getDriver()->createStatement( $sql );
		$result = $stmt->execute( $bind );

		$list = [];

		foreach( $result as $row ) {
			$album = new Album();
			$album->setId( $row[ 'id' ] )
			      ->setTitle( $row[ 'title' ] )
			      ->setArtist( $row[ 'artist' ] );

			$list[] = $album;
		}

		return $list;
	}

	public function insert( Album $album )
	{
		$sql = '
			INSERT INTO album (title, artist )
			VALUES( :title, :artist )
		';

		$bind = [
			'title'  => $album->getTitle(),
			'artist' => $album->getArtist(),
		];

		$this->db->getDriver()->createStatement( $sql )->execute( $bind );
	}

	public function update( $album )
	{
		$sql = '
			UPDATE album
			SET
				title = :title,
				artist = :artist
			WHERE
				id = :id
		';

		$bind = [
			'id'     => $album->getId(),
			'title'  => $album->getTitle(),
			'artist' => $album->getArtist(),
		];

		$this->db->getDriver()->createStatement( $sql )->execute( $bind );
	}

	public function delete( int $id )
	{
		$sql = '
			DELETE FROM album
			WHERE
				id = :id
		';

		$bind = [
			'id' => $id,
		];

		$this->db->getDriver()->createStatement( $sql )->execute( $bind );
	}
}