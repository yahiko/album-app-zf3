<?php
declare( strict_types = 1 );

namespace Album;

use Album\Mapper\AlbumMapper;
use Album\Service\AlbumService;
use Zend\Db\Adapter\AdapterInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\ServiceManager;

class Module implements ConfigProviderInterface
{
	public function getServiceConfig()
	{
		return [
			'factories' => [
				AlbumService::class => function ( ServiceManager $sm ) {
					$mapper  = $sm->get( AlbumMapper::class );
					$service = new AlbumService( $mapper );

					return $service;
				},

				AlbumMapper::class => function ( ServiceManager $sm ) {
					$db     = $sm->get( AdapterInterface::class );
					$mapper = new AlbumMapper( $db );

					return $mapper;
				},
			],
		];
	}

	public function getControllerConfig()
	{
		return [
			'factories' => [
				Controller\AlbumController::class => function ( $container ) {
					return new Controller\AlbumController(
						$container->get( AlbumService::class )
					);
				},
			],
		];
	}

	public function getConfig()
	{
		return [
			'router'       => [
				'routes' => [
					'album' => [
						'type'    => Segment::class,
						'options' => [
							'route'       => '/album[/:action[/:id]]',
							'constraints' => [
								'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'id'     => '[0-9]+',
							],
							'defaults'    => [
								'controller' => Controller\AlbumController::class,
								'action'     => 'index',
							],
						],
					],
				],
			],
			'view_manager' => [
				'template_path_stack' => [
					'album' => __DIR__ . '/../view',
				],
			],
		];
	}
}