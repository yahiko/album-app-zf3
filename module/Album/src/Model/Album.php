<?php
declare( strict_types = 1 );

namespace Album\Model;

class Album
{
	private $id;

	private $artist;

	private $title;

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 *
	 * @return Album
	 */
	public function setId( $id )
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getArtist()
	{
		return $this->artist;
	}

	/**
	 * @param mixed $artist
	 *
	 * @return Album
	 */
	public function setArtist( $artist )
	{
		$this->artist = $artist;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param mixed $title
	 *
	 * @return Album
	 */
	public function setTitle( $title )
	{
		$this->title = $title;

		return $this;
	}
}